package com.payload;

import java.util.ArrayList;
import java.util.List;


public class ApiResponse {
	 private Boolean success;
	    private String message;
	    private int id;
	    private String regKey;
	    private List <Integer> idList;
	   // private List<selectedProcesses> idList2;
	    private String ids;

	    public ApiResponse(Boolean success, String message,int id) {
	        this.success = success;
	        this.message = message;
	        this.setId(id);
	    }
	    
	    public ApiResponse(Boolean success, String message,int id,String regKey) {
	        this.success = success;
	        this.message = message;
	        this.id = id;
	        this.regKey = regKey;
	    }

	    public ApiResponse(Boolean success, String message) {
	        this.success = success;
	        this.message = message;
	    }
	    public ApiResponse(Boolean success, String message,String ids) {
	        this.success = success;
	        this.message = message;
	        this.ids = ids;
	    }
	    
	    public ApiResponse(boolean success, String message, ArrayList<Integer> idList) {
			// TODO Auto-generated constructor stub
	    	  this.success = success;
	          this.message = message;
	          this.idList = idList;
	    	
		}

		

		public ApiResponse(boolean success2, String message2, Long id2) {
			// TODO Auto-generated constructor stub
			 this.success = success2;
		        this.message = message2;
		        this.setId(id2.intValue());
		}

	
		/*
		 * public ApiResponse(boolean success1, String message1,
		 * ArrayList<selectedProcesses> idList2) { // TODO Auto-generated constructor
		 * stub this.success = success1; this.message = message1; this.idList2 =
		 * idList2; }
		 */

		public String getIds() {
			return ids;
		}

		public void setIds(String ids) {
			this.ids = ids;
		}

		public Boolean getSuccess() {
	        return success;
	    }

	    public void setSuccess(Boolean success) {
	        this.success = success;
	    }

	    public String getMessage() {
	        return message;
	    }

	    public void setMessage(String message) {
	        this.message = message;
	    }

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getRegKey() {
			return regKey;
		}

		public void setRegKey(String regkey) {
			this.regKey = regkey;
		}

		public List<Integer> getIdList() {
			return idList;
		}

		public void setIdList(List<Integer> idList) {
			this.idList = idList;
		}
		/*
		 * public List<selectedProcesses> getIdList2() { return idList2; }
		 * 
		 * public void setIdList2(List<selectedProcesses> idList2) { this.idList2 =
		 * idList2; }
		 */
		

}


