package com.example.demo.controller;

import java.net.URI;
import java.util.List;

import javax.imageio.spi.RegisterableService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.example.demo.model.Register;
import com.example.demo.service.RegisterService;
import com.payload.ApiResponse;

@RestController
@CrossOrigin
@RequestMapping("/reg")
public class RegisterController {

	@Autowired
	RegisterService ser;

	@GetMapping("/list")
	public List<Register> getAllDetails() {
		return ser.getAllDetails();

	}
	@GetMapping("/list/{id}")
	public Register getDetails(@PathVariable("id") int id) {
		return ser.getDetailsById(id);

	}
	@PostMapping("/add")
	public  ResponseEntity<ApiResponse> addDetails(@RequestBody Register reg) {
		Register r=ser.addDetails(reg);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(r.getId()).toUri();
		return ResponseEntity.created(location)
				.body(new ApiResponse(true, "Your Final Submit is Successfully Submit!",r.getId()));
	}
	
	@PutMapping("/update")
	public Register updateDetails(@RequestBody Register reg) {
		return ser.updateDetails(reg);

	}
	@DeleteMapping("/delete/{id}")
	public void deleteDetails(@PathVariable("id") int id) {
		ser.deleteDetails(id);

	}
	
}
