package com.example.demo.service;

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Register;
import com.example.demo.repo.RegistrationRepository;

@Service
public class RegisterServiceImp implements RegisterService {
	@Autowired
	RegistrationRepository registrationRepository;

	@Override
	public List<Register> getAllDetails() {
		// TODO Auto-generated method stub
		return registrationRepository.findAll();
	}

	@Override
	public Register addDetails(Register reg) {
		// TODO Auto-generated method stub
		Register r =new Register();
		r.setName(reg.getName());
		r.setPhone(reg.getPhone());
		r.setEmail(reg.getEmail());
		Register rr= registrationRepository.save(r);
		String[] recipients = new String[1];

		String from = "hareesh.k@tejoma.com";
		String password = "harish96";
		recipients[0] = rr.getEmail();
		//recipients[1] = rr.getEmail();

		String subject = "Account Credentials";
		String body = "UserName : " + rr.getEmail() + "\n" + "Password : "
				+ rr.getName();
		sendMail(from, password, recipients, subject, body);
		return r;
	}
	private void sendMail(String from, String password, String[] recipients, String subject, String body) {
		// TODO Auto-generated method stub

		for (String emailString : recipients) {
			String host = "smtp.gmail.com";

			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", "587");

			// Get the Session object
			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(from, password);
				}
			});

			try {
				// Create a default MimeMessage object
				Message message = new MimeMessage(session);

				message.setFrom(new InternetAddress(from));
				/*
				 * message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
				 */
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailString));
				// Set Subject
				message.setSubject(subject);

				// Put the content of your message
				message.setText(body);

				// Send message
				Transport.send(message);

				System.out.println("Sent message successfully....");

			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}

		}

	}

	@Override
	public Register updateDetails(Register reg) {
		// TODO Auto-generated method stub
		
		return registrationRepository.save(reg);
	}

	@Override
	public void deleteDetails(int id) {
		// TODO Auto-generated method stub
		registrationRepository.deleteById(id);
	}

	@Override
	public Register getDetailsById(int id) {
		// TODO Auto-generated method stub
		return registrationRepository.findById(id).get();
	}

}
