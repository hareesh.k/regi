package com.example.demo.service;


import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.model.Register;


@Service
public interface RegisterService {

	public List<Register> getAllDetails();
	public Register addDetails(Register reg);
	public Register updateDetails(Register reg);
	public void deleteDetails(int id);
	public Register getDetailsById(int id);
}

